<?php
/** URLS return's an array that contains:
    Array Key : the route pattern controllername:action  
    Array Value:  the controller's name
    You may use an array for the Array Value mentioned above in 
    order to route to a custom controller action as well.
**/
return $urls = [
	"site:search" => ["Site", 'search']
];
