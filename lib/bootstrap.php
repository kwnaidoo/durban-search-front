<?php 
// include , execute and store the dispathcers resulting array.
$route_info = include(BASE_PATH."lib/router.php");

//loads the array off urls
$urls = include(BASE_PATH."urls.php");
//load our base controller , as all other controllers will extend this
//load our API class as nearly every controller will communicate to the API
require_once(BASE_PATH."lib/BaseController.php");
require_once(BASE_PATH."lib/WebserviceApi.php");

/** setup autoloading - this will automatically import the controller class from
    our controllers directory so that we avoid a million and one 
    require_once calls . pattern is classname.php e.g. Site.php
**/
function __autoload($class_name) {
    include BASE_PATH."controllers/".$class_name.'.php';
}


// : depicts no controller or action requested , i.e. this must be the homepage
if($route_info['route'] == ":"){
	require_once(BASE_PATH."themes/default/home.html");
	die;
}

/** check if our urls array has a key matching the route from our 
    route_info; format: controller:action **/

if(array_key_exists($route_info['route'], $urls)){
		/** since a route was defined in our urls.php file,
		    we now must instantiate the controller associated
		    with this route.
		**/

		/** URL's will always contain the controller's name
		    however in some instances URL's may also store
		    the actions name, this poses a problem since
		    the array value for this route could either be
		    an array (if action is defined) OR a string (
		    if only controller is defined). So this
		    logic will determine the data type and 
		    get the correct action and correctly.
		**/

		if (is_array($urls[$route_info['route']])){
			
			//controllers always come first so they'll always be at position 0
			$controller_class = $urls[$route_info['route']][0];
			$action_name = $urls[$route_info['route']][1];

		}else{
			$controller_class = $urls[$route_info['route']];
			//no custom action so just use what came in the URL
			$action_name = $route_info['action'];
		}

		$controller = new $controller_class();
		
		/** Since the URLS logic dynamically maps the 
			controller and action - it's not guranteed that
			the controller and action set in the URL will
			match the relevant class and method names.
			Therefore we store the correct controller and
			action names in controller and action in our array
			and add two new entries into our array to store
			what came in the URL.
		**/
		$route_info['url_controller'] = $route_info['controller'];
		$route_info['url_action'] = $route_info['action'];
		$route_info['controller'] = $controller_class;
		$route_info['action'] = $action_name;

		// store the route information in our controller.
		$controller->route_info = $route_info;

		/**  now execute the before_filter  - this is a special method
		     declared in BaseController that will always run before
		     any controller action is exectued (excluding the constructor).
		     This enables you to apply logic to every single request before
		     any action can do it's work. Perfect for a login system where 
		     you want to check if the user has access to the controller action
		     they requesting. 
		**/
		$controller->before_filter();

		//now execute the action method
		$controller->$action_name();
}
else{
	require_once(BASE_PATH."themes/default/404.html");
}