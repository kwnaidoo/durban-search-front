<?php 
/** This is an abstract class because it's not intended to be used directly 
	but rather serves as base for all other controllers to extend.
**/
abstract class BaseController{

	//The api class - this is used to communicate to the backend webservice
	protected $api = None;

	public $route_info = [];
	//will store whatever information that needs to be sent to the view.
	protected $data = [];
	protected $theme = "default";

	function __construct(){

		$this->api = new  WebserviceApi();
	}

	/** These filters allow for hooking into controller actions and executing
	    logic before and after a controller action:
	    before_filter : executed before any action executes
	    before_render : execute after a controllers action but before any view
	     				logic can be processed.
	    before_view : executed just after the header part off the theme is loaded
	    after view : just after the content is rendered but before the footer
	    			 is rendered.
	**/
	public function before_filter(){}
	protected function before_render(){}
	protected function before_view(){}
	protected function after_view(){}

	/** Render takes in a view name and builds the themes
	    to return to the browser, every theme must be stored
	    in the themes directory and contain two files
	    namely header.php and footer.php e.g. 
	    themes/my-awesome-themes/header.php
	    							  footer.php
	    All assets for the theme should be stored in 
	    a subfolder under public/themes, the subfolder name
	    should be exactly the same as that off the theme.

	    All view files need to be placed under a subfolder inside
	    the views directory , the subfolder name should be exactly
	    the same of that off the controller class e.g. Site.
	    Alternatively - you can place your view file under
	    views/shared 
	**/

	protected function render($view_tpl){
		$this->before_render();
		explode($this->data);
		/** store common variables that may be useful
			in our views, note we prefix controller and
			action with route to prevent any name clashes.
		**/
		$theme_url = BASE_URL."public/{$this->theme}/";
		$route_controller = $this->route_info['controller'];
		$route_action = $this->route_info['controller'];

		require_once(BASE_PATH."themes/{$this->theme}/header.php");
		$this->before_view();
		
		$view_path = BASE_PATH."views/{$route_controller}/{$view_tpl}.php";
		/** first check if there is a view file for this controller,
		    if none exists than there is probably a shared view file
		    for this action so require that one instead. 
		**/
		if(file_exists($view_path)){
			require_once($view_path);
		}else{
			require_once(BASE_PATH."views/shared/{$view_tpl}.php");
		}

		$this->after_view();
		require_once(BASE_PATH."themes/{$this->theme}/footer.php");



	}

}