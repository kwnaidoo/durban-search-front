<?php
class WebserviceApi{
	//meta will store all the API request headers sent back
	public $meta = null;

	public $status_code = null;

	/** execute is the method in charge off sending a request to an API
	    endpoint and returning a response , this method accepts two arguments:
	    $webservice - the method to trigger e.g. get_users
	    $data - A PHP array of values to send to the API endpoint
		
	**/

	function execute($webservice ='' , $data=[]){
		//build our full restful endpoint
		$endpoint = API_ENDPOINT.$webservice;
		//generate our autentication token using the API key and client secret
		$data['token'] = sha1(API_KEY.CLIENT_SECRET);

		//headers and content to be sent to the API
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        // FORMAT the PHP array into POST data safe for sending over HTTP
		        'content' => http_build_query($data),
		        //safer handling off errors to prevent throwing PHP warnings
		        'ignore_errors' => true
		    ),
		);
		//setup our stream context - this is required to send data with file_get_contents
		$ctx  = stream_context_create($options);
		//trigger the API
		$response = file_get_contents($endpoint, null, $ctx);

		//store relevant reponse information and return API data
		$this->meta = $http_response_header;
		$this->status_code = $this->status_code();
		return $response;
	}

	//extracts the numerical HTTP status code from headers
	function status_code(){
		$status = 500;
		if(isset($this->meta[0])){
		    preg_match( "#HTTP/[0-9\.]+\s+([0-9]+)#",$this->meta[0], $s);
		    if(isset($s[1])){
		    	$status = $s[1];
		    }
		}

		return (int)$status;
	}

}