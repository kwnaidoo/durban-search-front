<?php
// get the current requested URL
$uri = $_SERVER['REQUEST_URI'];

//convert the string to an array, seperating by the slash
$uri = explode("/", $uri);


$controller = null;
$action = null;

// position 1 - controller , position 2 - action
if(isset($uri[1]))
    $controller = $uri[1];
if(isset($uri[2]))
    $action = $uri[2];

// delete the action & controller (0 - will always be blank because it's the data before the first /")
if(isset($uri[0]))
    unset($uri[0]);
if (isset($uri[1]))
    unset($uri[1]);
if(isset($uri[2]))
    unset($uri[2]);

//rest of the uri would be get params
$args = $uri;

// return route information when this module is called
return [
    "controller" => $controller, 
    "action" => $action, 
    "args" =>array_values($args), // array_values used to re-index the array
    "route" => "{$controller}:{$action}"
];